<?php

// src/AppBundle/Controller/CompanyController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Company;
use AppBundle\Form\CompanyType;

class CompanyController extends Controller
{
	/**
	 * @Route("/companies/add", name="companies_add")
	 */
	public function addAction(Request $request)
	{
		$company = new Company(); // Preparar entidad sin inicializar para el formulario
		
		// Crear formulario
		$form = $this->createForm(CompanyType::class, $company, array('submit_label' => 'Agregar', 'return_url' => '/companies/view'));

		// Procesar pedido para el formulario
		$form->handleRequest($request);

		if ( $form->isSubmitted() && $form->isValid() ) // Si el formulario fue recibido y es válido
		{
			$company = $form->getData(); // Obtener datos de la entidad del formulario

			// Persistir la entidad
			$doctrinemanager = $this->getDoctrine()->getManager();
		
			$doctrinemanager->persist($company);

			$doctrinemanager->flush();

			// Retornar a la vista de compañías
			return $this->redirectToRoute('companies_view');
		}

		// Devolver la página correspondiente
		return $this->render('company/add.html.twig', array(
			'form' => $form->createView()
		));
	}

	/**
	 * @Route("/companies/modify", name="companies_modify")
	 */
	public function modifyAction(Request $request)
	{
		$company_id = $request->query->get('id'); // Obtener id de compañía		

		if ( isset( $company_id) ) // Si hay id de compañía
		{
			$company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($company_id); // Obtener la compañía en la base de datos

			if ( isset($company) ) // Si la compañía se obtuvo (Existe)
			{
				// Crear formulario
				$form = $this->createForm(CompanyType::class, $company, array('submit_label' => 'Modificar', 'return_url' => '/companies/view'));
			
				// Procesar pedido para el formulario
				$form->handleRequest($request);

				if ( $form->isSubmitted() && $form->isValid() ) // Si el formulario fue recibido y es válido
				{
					// Modificar la compañía con los nuevos datos del formulario
					$company = $form->getData();

					$doctrinemanager = $this->getDoctrine()->getManager();

					$doctrinemanager->flush();

					return $this->redirectToRoute('companies_view'); // Redirigir a la página de vista de compañías
				}
				else // Caso contrario el usuario entró a la página de modificación, pero sin haber modificado nada aún
				{
					// Enviar la página correspondiente
					return $this->render('company/modify.html.twig', array(
						'form' => $form->createView()
					));
				}
			}
			else // Si no existe la compañía
			{
				return new Response("Error: La compañía no existe", Response::HTTP_NOT_FOUND);
			}
		}
		else // Si falta el id de compañía
		{
			return new Response("Error: Falta el id de compañía", Response::HTTP_BAD_REQUEST);
		}
	}

	/**
	 * @Route("/companies/remove", name="companies_remove")
	 */
	public function removeAction(Request $request)
	{
		$company_id = $request->query->get('id'); // Obtener id de compañía
		
		if ( isset( $company_id ) ) // Si hay id de compañía
		{
			// Obtener compañía de la base de datos con el id especificado
			$company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($company_id);

			if ( isset($company) ) // Si se obtuvo (Se encontró)
			{
				// Eliminar la compañía
				$doctrinemanager = $this->getDoctrine()->getManager();
				$doctrinemanager->remove($company);
				$doctrinemanager->flush();

				// Redirigir a la página que dirigió a ésta
				return $this->redirect($request->headers->get('referer'));		
			}
			else // Si no se encontró
			{
				return new Response("Error: La compañía no existe", Response::HTTP_NOT_FOUND);
			}
		}
		else // Si no hay id de compañía
		{
			return new Response("Error: Falta especificar el id de compañía", Response::HTTP_BAD_REQUEST);
		}
	}

	/**
	 * @Route("/companies/view", name="companies_view")
	 */
	public function viewAction(Request $request)
	{
		// Obtener el 'fromId', si no se ingresó obtener el 'toId'
		$getVariables = $request->query->get('fromId');		

		$fromId = $request->query->get('fromId');

		if ( !isset($fromid) )
		{
			$toId = $request->query->get('toId');
		}
		
		$maxEntries = 10; // No más de 10 entradas por página
		
		// Formar la petición al 'doctrine'
		$repository = $this->getDoctrine()->getRepository('AppBundle:Company');
		$queryBuilder = $repository->createQueryBuilder('c');
		
		$queryBuilder->setMaxResults($maxEntries);

		if ( isset( $fromId ) )
		{
			$queryBuilder
				->where('c.id >= :fromId')
				->setParameter('fromId', $fromId)
			;
		}
		else if ( isset( $toId ) )
		{
			$queryBuilder
				->where('c.id <= :toId')
				->setParameter('toId', $toId)
			;
		}

		/**
		 * Si es hasta un id determinado se obtiene en orden descendente y se vuelve a reestablecer el orden
		 * para garantizar que se obtienen las compañías más cercanas o igual al id máximo
		 */

		// Si se indicó hasta un id determinado, obtenerlos en orden descendente, caso contrario en orden ascendente
		if ( isset( $toId ) )
		{
			$queryBuilder->orderBy('c.id', 'DESC');
		}
		else
		{
			$queryBuilder->orderBy('c.id', 'ASC');
		}

		// Realizar petición y obtener resultado
		$companies = $queryBuilder->getQuery()->getResult();

		// Invertir el orden de las compañías recibidas, si es hasta un id determinado
		if ( isset( $toId ) )
		{
			$companies = array_reverse($companies, true);
		}

		// Mapeo entre campos de base de datos y etiquetas de página
		$fields = array(
			array('id', 'Id'),
			array('cuit', 'Cuit'),
			array('name', 'Nombre'),
			array('numberOfEmployees', 'Número de empleados')
		);
		
		/**
		 * Convierte la representación de compañías a arrays y obtiene
		 * el primer id y el último id de las compañías obtenidas
		 */
		foreach ( $companies as $eachCompany )
		{
			$companiesArray[] = array(
				'id' => $eachCompany->getId(),
				'cuit' => $eachCompany->getCuit(),
				'name' => $eachCompany->getName(),
				'numberOfEmployees' => $eachCompany->getNumberOfEmployees()
			);
		
			if ( ! isset( $firstId ) )
			{
				$firstId = $eachCompany->getId();
			}

			$lastId = $eachCompany->getId();
		}

		// Deja en ésta variable la cantidad de compañías recibidas
		$companiesLength = sizeof($companies);

		// Preparar parámetros de la plantilla twig
		$twigParameters=array(
			'fields' => $fields,
			'companies' => $companiesArray
		);

		if ( ( $companiesLength == $maxEntries ) || isset( $fromId ) )
		{
			$twigParameters['beforeLinkToId'] = $firstId;
		}

		if ( ( $companiesLength == $maxEntries ) || isset( $toId ) )
		{
			$twigParameters['afterLinkFromId'] = $lastId;
		}

		// Devolver la página correspondiente
		return $this->render('company/view.html.twig', $twigParameters);
	}
}

