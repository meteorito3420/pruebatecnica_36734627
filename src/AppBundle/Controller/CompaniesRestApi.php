<?php

// src/AppBundle/Controller/CompaniesRestApi.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Company;
use AppBundle\Form\CompanyType;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CompaniesRestApi extends Controller
{
	/**
	 * @Route("/restapi/companies", name="company_rest_api")
	 */
	public function restApiAction(Request $request) {
		$company_id = $request->query->get('id'); // Obtiene el posible id de compañía
		
		$encoders = array(new XmlEncoder(), new JsonEncoder());
		$normalizers = array(new ObjectNormalizer());

		$serializer = new Serializer($normalizers, $encoders);

		if ( isset( $company_id ) ) // Si se especificó id de compañía
		{
			// Obtener la compañía de la base de datos con el id especificado
			$company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($company_id);
			
			if ( isset( $company ) ) // Si fue encontrada
			{
				$unencodedReturnData = $company; // El contenido a codificar es la compañía
				$returnValue = Response::HTTP_OK; // Indicar que fue exitoso
			}
			else // Si no fue encontrada
			{
				$returnString = "Error: La compañía no existe"; // Devolver descripción de error
				$returnValue = Response::HTTP_NOT_FOUND; // Indicar que no fue encontrada
			}
		}
		else // Si no se especificó id de compañía
		{
			// Obtener todas las compañías
			$repository = $this->getDoctrine()->getRepository('AppBundle:Company');
			$queryBuilder = $repository->createQueryBuilder('c');
			$unencodedReturnData = $repository->createQueryBuilder('c')->getQuery()->getResult();

			// Indicar que fue exitoso
			$returnValue = Response::HTTP_OK;
		}

		// Si no se indicó salida significa que la salida tiene que ser la información a codificar (Sin codificar), en JSON
		if ( !isset( $returnString ) )
		{
			$returnString = $serializer->serialize($unencodedReturnData, 'json');
		}

		return new Response($returnString, $returnValue);
	}
}
