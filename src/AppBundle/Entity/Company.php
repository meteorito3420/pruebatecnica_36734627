<?php

// src/AppBundle/Entity/Company.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="company")
 */
class Company
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @Assert\NotBlank()
	 * @Assert\Length(max=100)
	 */
	private $cuit;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @Assert\NotBlank()
	 * @Assert\Length(max=100)
	 */
	private $name;

	/**
	 * @ORM\Column(type="integer")
	 * @Assert\GreaterThanOrEqual(0)
	 */
	private $numberOfEmployees;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set cuit
	 *
	 * @param string $cuit
	 * @return Company
	 */
	public function setCuit($cuit)
	{
		$this->cuit = $cuit;

		return $this;
	}

	/**
	 * Get cuit
	 *
	 * @return string 
	 */
	public function getCuit()
	{
		return $this->cuit;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Company
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set numberOfEmployees
	 *
	 * @param integer $numberOfEmployees
	 * @return Company
	 */
	public function setNumberOfEmployees($numberOfEmployees)
	{
		$this->numberOfEmployees = $numberOfEmployees;

		return $this;
	}

	/**
	 * Get numberOfEmployees
	 *
	 * @return integer 
	 */
	public function getNumberOfEmployees()
	{
		return $this->numberOfEmployees;
	}
}
