<?php

// src/AppBundle/Form/CompanyType.php
namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
	// Construye formulario
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('cuit', TextType::class, array('label' => 'Cuit'))
			->add('name', TextType::class, array('label' => 'Nombre'))
			->add('numberOfEmployees', IntegerType::class, array('label' => 'Cantidad de empleados'))
			->add('save', SubmitType::class, array('label' => $options['submit_label'], 'attr' => array('style' => 'float: left') ))
			->add('cancel', ButtonType::class, array('label' => 'Volver', 'attr' => array('onclick' => 'parent.location=\''.$options['return_url'].'\'')))
		;
	}

	// Configura las opciones 'submit_label' y 'return_url' como requeridas (Son personalizadas, propias de ésta implementación)
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setRequired(array('submit_label'));
		$resolver->setRequired(array('return_url'));
	}
}
